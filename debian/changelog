simplegeneric (0.8.1-5+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Mon, 10 Jul 2023 16:07:07 +0000

simplegeneric (0.8.1-5) unstable; urgency=medium

  * Team upload.
  [ Debian Janitor ]
  * Update standards version to 4.1.5, no changes needed.

  [ Athos Ribeiro ]
  * Run tests with pytest instead of setup.py test.

 -- Athos Ribeiro <athoscribeiro@gmail.com>  Mon, 05 Dec 2022 20:56:50 +0000

simplegeneric (0.8.1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Bump debhelper from old 12 to 13.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 20:18:52 -0400

simplegeneric (0.8.1-3apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 22:25:36 +0000

simplegeneric (0.8.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938483

 -- Sandro Tosi <morph@debian.org>  Wed, 01 Apr 2020 17:41:02 -0400

simplegeneric (0.8.1-2co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 22 Feb 2021 02:24:41 +0000

simplegeneric (0.8.1-2) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field

  [ Piotr Ożarowski ]
  * Add dh-python to Build-Depends

  [ Daniele Tricoli ]
  * Ignore egg-info.
  * debian/watch
    - Switch to pypi.debian.net.
  * debian/compat
    - Bump debhelper compat level to 11.
  * debian/control
    - Bump debhelper version to >= 11.
    - Bump Standards-Version to 4.1.4 (no changes needed).
    - Add autodep8 tests.
  * debian-copyright
    - Update copyright years.

 -- Daniele Tricoli <eriol@mornie.org>  Wed, 23 May 2018 00:34:43 +0200

simplegeneric (0.8.1-1) unstable; urgency=low

  * New upstream release (Closes: #640531)
    - With Python 3 support
  * Builded Python 3 package
  * debian/control
    - Removed Cédric and added Debian Python Modules Team to Maintainer
      Thanks to Cédric Delfosse for his past work!
    - Added myself to Uploaders (Closes: #668915)
    - Bumped Standards-Version to 3.9.3 (no changes needed)
    - Removed Provides: ${python:Provides}
    - Removed ${shlibs:Depends} from python-simplegeneric's Depends
      field
    - Switched Priority to optional
    - Added Vcs-* fields
  * debian/{control,rules}
    - Switched to dh_python2. Thanks to Julian Taylor for the report
      and the patch (Closes: #631408)
  * debian/copyright
    - Made DEP5 compliant
    - Upstream switched to ZPL-2.1
  * debian/rules
    - Clean properly to build packages twice in a row
    - Run tests at build time

 -- Daniele Tricoli <eriol@mornie.org>  Fri, 04 May 2012 12:51:10 +0200

simplegeneric (0.7-1) unstable; urgency=low

  * Initial release (Closes: #591295)

 -- Cédric Delfosse <cedric@debian.org>  Wed, 04 Aug 2010 20:53:42 +0200
